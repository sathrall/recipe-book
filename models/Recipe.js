const { Schema, model } = require('mongoose')

const RecipeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    ingredients: {
        type: Array,
        default: []
    },
    steps: {
        type: Array,
        default: []
    }
})

const Recipe = model('recipe', RecipeSchema)

module.exports = Recipe