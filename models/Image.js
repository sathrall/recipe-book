const { Schema, model } = require('mongoose')

const ImageSchema = new Schema({
    name: {
        type: String,
        default: "none",
        required: true
    },
    data: {
        type: String
    }
})

const Image = model('image', ImageSchema)

module.exports = Image;