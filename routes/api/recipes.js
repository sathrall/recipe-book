const { Router } = require('express')
const Recipe = require('../../models/Recipe')

const router = Router()

router.get('/', async (req, res) => {
    try {
        const recipes = await Recipe.find()
        if (!recipes) throw new Error('No recipes')
        const sorted = recipes.sort((a, b) => {
            return new Date(a.date).getTime() - new Date(b.date).getTime()
        })
        res.status(200).json(sorted)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.get('/:id', async (req, res) => {
    const { id } = req.params

    try {
        const recipe = await Recipe.findById(id, req.body)
        if (!recipe) throw new Error('No recipes')
        res.status(200).json(recipe)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.post('/', async (req, res) => {
    const newRecipe = new Recipe(req.body)
    
    try {
        const recipe = await newRecipe.save()
        if (!recipe) throw new Error('Something went wrong while saving the recipe')
        res.status(200).json(recipe)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.put('/:id', async (req, res) => {
    const { id } = req.params

    try {
        const response = await Recipe.findByIdAndUpdate(id, req.body)
        if (!response) throw new Error('Something went wrong while updating the recipe')
        const updated = { ...response._doc, ...req.body }
        res.status(200).json(updated)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.delete('/:id', async (req, res) => {
    const { id } = req.params

    try {
        const removed = await Recipe.findByIdAndDelete(id)
        if (!removed) throw new Error('Something went wrong while removing the recipe')
        res.status(200).json(removed)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

module.exports = router