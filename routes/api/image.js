const { Router } = require('express')
const Image = require('../../models/Image')
const ImageRouter = Router()
const multer = require('multer')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/webp') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
})

ImageRouter.route('/upload')
    .post(upload.single("imageData"), (req, res, next) => {
        const newImage = new Image({
            name: req.body.imageName,
            data: req.body.imageData
        })

        newImage.save()
            .then((result) => {
                res.status(200).json({
                    success: true,
                    document: result
                })
            })
            .catch(err => next(err))
    })

module.exports = ImageRouter;